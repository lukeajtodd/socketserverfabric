var https = require('https');
var fs = require('fs');
var options = {
    key: fs.readFileSync('/etc/nginx/ssl/sockets.devsqd.com/298242/server.key'),
    cert: fs.readFileSync('/etc/nginx/ssl/sockets.devsqd.com/298242/server.crt'),
}
var server = https.createServer(options, function(req,res){}),
    io = require('socket.io').listen(server);

var history = {};
var future = {};
var boards = [];
var pointer = 0;
var port = 3344;

function addHistory ({ uid, board }) {
    if (history[uid].length > 29) {
        history[uid].shift();
    }
    history[uid].push(board);
    future[uid] = [];
}

io.on('connection', function (socket) {

    console.info('Client connected');

    // JUST FOR TESTING
    // SHOULD NEVER NEED WHOLE STATE IN APP
    socket.on('state', () => {
        socket.emit('state', { history, future });
    });

    socket.emit('load', boards);
    socket.broadcast.emit('resetSelections');
    socket.emit('resetObjects');

    socket.on('board:new', ({ uid, canvas }) => {
        boards.push({ uid, canvas });
        socket.broadcast.emit('load', boards);
        history[uid] = [];
        history[uid].push(canvas);
        future[uid] = [];
    });
    
    socket.on('board:remove', uid => {
        let newBoards = boards.filter(board => {
            return board.uid !== uid
        });
        boards = newBoards;
        io.emit('load', boards);
        delete history[uid];
        delete future[uid];
    });

    socket.on('change:addition', ({ eraser, uid, obj, canvas }) => {
        boards.forEach((board, index) => {
            if (board.uid === uid) {
                addHistory({ uid, board: canvas });
                boards[index] = { uid, canvas };
                socket.broadcast.emit('object:addition', { obj, uid, canvas });
            }
        });
    });

    socket.on('change:removal', ({ uid, objId, canvas }) => {
        boards.forEach((board, index) => {
            if (board.uid === uid) {
                addHistory({ uid, board: canvas });
                boards[index] = { uid, canvas };
                socket.broadcast.emit('object:removal', { objId, uid, canvas });
            }
        });
    });

    socket.on('change:modify', ({ uid, obj, canvas }) => {
        boards.forEach((board, index) => {
            if (board.uid === uid) {
                addHistory({ uid, board: canvas });
                boards[index] = { uid, canvas };
                console.log(obj);
                socket.broadcast.emit('object:modify', { obj, uid, canvas });
            }
        });
    });

    socket.on('object:lock', ({ uid, objs, canvas }) => {
        socket.broadcast.emit('object:lock', { uid, objs });
    });

    socket.on('object:unlock', ({ uid, objs, canvas }) => {
        socket.broadcast.emit('object:unlock', { uid, objs });
    });

    socket.on('board:update', ({ uid, canvas }) => {
        boards.forEach((board, index) => {
            if (board.uid === uid) {
                boards[index] = { uid, canvas };
            }
        });
    })

    socket.on('undo', ({ uid }) => {
        if (history[uid].length > 1) {
            future[uid].push(history[uid].pop());
            currBoard = history[uid][history[uid].length - 1];
            io.emit('refresh', { uid, board: currBoard });
        }
    });

    socket.on('redo', ({ uid, board }) => {
        if (future[uid].length > 0) {
            if (history[uid].length > 30) {
                history[uid].shift();
            }
            history[uid].push(future[uid].pop());
            currBoard = history[uid][history[uid].length - 1];
            io.emit('refresh', { uid, board: currBoard });
        }
    });

    socket.on('disconnect', function() {
        console.info('Client disconnected');
    });

});

server.listen(port, function() {
    console.log('Listening on *:' + port);
});