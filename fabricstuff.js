var socket = io.connect('sockets.devsqd.com:3344', {
        secure: true,
        reconnect: true,
        rejectUnauthorized : false
    });
    socket.emit('client:setup', { id: window.classId });
    var canvas = new fabric.Canvas('whiteboard-canvas-1');

    var height = $(document).height();
        height = height - $('.navbar').height();
        height = height - $('.btn-toolbar').height();
        height = height - $('.toolbar-row').height();
        height = height - 150;

    canvas.setWidth($('.whiteboard-container').width());
    canvas.setHeight($('.whiteboard-container').height());

    fabric.Canvas.prototype.getObjectById = function (id) {
        var objs = this.getObjects();
        for (var i = 0, len = objs.length; i < len; i++) {
            if (objs[i].id == id) {
                return objs[i];
            }
        }
        return 0;
    };

    fabric.Object.prototype.toObject = (function(toObject) {
        return function() {
            return fabric.util.object.extend(toObject.call(this), {
                id: this.id
            });
        };
    })(fabric.Object.prototype.toObject);

    function changeHandler (e) {
        socket.emit('change', { obj: e.target.toJSON(['id']), canvas: canvas.toJSON(['id']) });
    }

    function deleteHandler (e) {
        socket.emit('delete', { objId: e.target.id, canvas: canvas.toJSON(['id']) });
    }

    function coordHandler () {
        canvas.getObjects().map(function(o) {
            return o.setCoords();
        });
    }

    canvas.add(new fabric.Rect({
        left: 100,
        top: 100,
        fill: 'red',
        width: 20,
        height: 20,
        id: guid()
    }));

    document.querySelector('.shapes--square').addEventListener('click', function (e) {
        console.log(canvas);
        canvas.add(new fabric.Rect({
            left: 100,
            top: 100,
            fill: 'red',
            width: 20,
            height: 20,
            id: guid()
        }));
        canvas.renderAll();
        console.log(canvas);
    });

    // document.querySelector('.delete-block').addEventListener('click', function (e) {
    //     let activeObject = canvas.getActiveObject();
    //     canvas.remove(activeObject);
    // })

    // document.querySelector('.lock-all').addEventListener('click', function (e) {
    //     if (canvas.selection === true) {
    //         canvas.selection = false;
    //         canvas.forEachObject(function(o) {
    //             o.selectable = false;
    //         });
    //         document.getElementById('canv').style.opacity = '.3';
    //     } else {
    //         canvas.selection = true;
    //         canvas.forEachObject(function(o) {
    //             o.selectable = true;
    //         });
    //         document.getElementById('canv').style.opacity = 1;
    //     }
    //     canvas.deactivateAll().renderAll();
    // });

    canvas.on('object:moving', function (e) {
        let obj = canvas.getObjectById(e.target.id);
        if (obj.lockMovementX === false && obj.lockMovementY === false) {
            socket.emit('lockdown', e.target.id);
        }
    });

    canvas.on('object:removed', deleteHandler);
    canvas.on('object:modified', changeHandler);
    canvas.on('path:created', changeHandler);
    canvas.on('object:added', changeHandler);

    socket.on('lockdown', function (id) {
        let obj = canvas.getObjectById(id);
        obj.set({
            selectable: false
        });
        canvas.renderAll();
    });

    socket.on('load', function (data) {
        canvas.loadFromJSON(data);
    });

    socket.on('object:pass', function (obj) {
        if (canvas.getObjectById(obj.id) === 0) {
            let klass = fabric.util.getKlass(obj.type);
            canvas.add(klass.fromObject(obj));
            coordHandler();
            canvas.renderAll();
        } else {
            canvas.getObjectById(obj.id).set(Object.assign(obj, {
                selectable: true
            }));
            coordHandler();
            canvas.renderAll();
        }
    });

    socket.on('object:remove', function (id) {
        let obj = canvas.getObjectById(id);
        if (obj !== 0) {
            canvas.remove(obj);
            canvas.renderAll();
        }
    });